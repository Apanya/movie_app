package com.android.example.github.service.Persistence;


import android.arch.persistence.room.Database;
import android.arch.persistence.room.RoomDatabase;

import com.android.example.github.service.model.Movies;


@Database(entities = { Movies.Results.class}, version = 4,exportSchema = false)
public abstract class  MoviesDatabase extends RoomDatabase {

    abstract  public  MoviesDao moviesDao();

    //private static MoviesDatabase INSTANCE;

   /* public static MoviesDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (MoviesDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            MoviesDatabase.class, "MoviesDb")
                            // Wipes and rebuilds instead of migrating if no Migration object.
                            // Migration is not part of this codelab.
                            .fallbackToDestructiveMigration()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }




    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase                   .Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                }
            };*/

        }



