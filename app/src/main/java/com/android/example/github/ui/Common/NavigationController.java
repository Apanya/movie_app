/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package com.android.example.github.ui.Common;
import com.android.example.github.R;
import com.android.example.github.view.UI.MainActivity;
//import com.android.example.github.view.UI.MoviesFragment;
import com.android.example.github.view.UI.MoviesListFragment;

import android.support.v4.app.FragmentManager;
import javax.inject.Inject;


public class NavigationController {
    private final int containerId;
    private final FragmentManager fragmentManager;
    @Inject
    public NavigationController(MainActivity mainActivity) {
        this.containerId = R.id.container;
        this.fragmentManager = mainActivity.getSupportFragmentManager();
    }

    public void navigateToMoviesList() {
        MoviesListFragment moviesListFragment = new MoviesListFragment();
        fragmentManager.beginTransaction()
                .replace(containerId, moviesListFragment)
                .commitAllowingStateLoss();
    }

   /* public void navigateToMovieDetails() {
        MoviesFragment fragment = new MoviesFragment();
        fragmentManager.beginTransaction()
                .replace(containerId, fragment)
                .addToBackStack(null)
                .commitAllowingStateLoss();
    }*/

}
