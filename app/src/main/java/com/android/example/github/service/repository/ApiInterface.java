package com.android.example.github.service.repository;

import com.android.example.github.service.model.Movies;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {
    String USGS_URL = "https://api.themoviedb.org/";
    //3/movie///popular?api_key=b8b9e99d11eb150d1abc559f9056a344&language=en-US&page=1";

    @GET("/3/movie/popular")
   Call<Movies> getMoviesList(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") int page);


    @GET("/3")
    Call<Movies> getMoviesDetails();
}


