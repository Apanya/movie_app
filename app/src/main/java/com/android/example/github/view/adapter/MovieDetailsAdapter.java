package com.android.example.github.view.adapter;

import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.android.example.github.R;
import com.android.example.github.service.model.Movies;
import com.android.example.github.ui.Common.DataBoundListAdapter;
import com.android.example.github.view.callback.MoviesClickCallback;
import com.android.example.github.databinding.MoviesListItemBinding;

import java.util.Objects;

public class MovieDetailsAdapter extends DataBoundListAdapter<Movies.Results, MoviesListItemBinding> {

    DataBindingComponent dataBindingComponent;

   public MovieDetailsAdapter(DataBindingComponent dataBindingComponent) {

       this.dataBindingComponent=dataBindingComponent;

    }


    @Override
    protected MoviesListItemBinding createBinding(ViewGroup parent) {

        MoviesListItemBinding binding = DataBindingUtil
                .inflate(LayoutInflater.from(parent.getContext()), R.layout                                 .movies_details_item, parent, false,dataBindingComponent);

        Movies.Results movies = binding.getResults();
        return binding;
    }

    @Override
    protected void bind(MoviesListItemBinding binding, Movies.Results item) {
       binding.setResults(item);
       }

    @Override
    protected boolean areItemsTheSame(Movies.Results oldItem, Movies.Results newItem) {
        Log.d("areitemsthesame","activity");
        return Objects.equals(oldItem.getId(), newItem.getId());

    }

    @Override
    protected boolean areContentsTheSame(Movies.Results oldItem, Movies.Results newItem) {
        Log.d("areContentstheSame","activity");
        return Objects.equals(oldItem.getId(), newItem.getId());

    }


}
