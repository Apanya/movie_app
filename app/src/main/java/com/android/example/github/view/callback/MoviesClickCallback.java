package com.android.example.github.view.callback;


import com.android.example.github.service.model.Movies;

public interface MoviesClickCallback {
    void onClick(Movies.Results movies);
}
