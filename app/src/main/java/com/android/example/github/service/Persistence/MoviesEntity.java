package com.android.example.github.service.Persistence;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;
import android.support.transition.Transition;


@Entity(tableName = "Movies1")
public  class MoviesEntity{
        @NonNull
        @PrimaryKey
        @ColumnInfo(name = "id")
        public int mId ;

        @ColumnInfo(name = "title")
        public String mTitle ;

        @ColumnInfo(name = "popularity")
        public String mPopularity;

        @ColumnInfo(name = "overView")
        public String  mOverView;



    public int getmId() {
        return mId;
    }


    public void setmId(int mId) {
        this.mId = mId;
    }


    public String getmTitle() {
        return mTitle;
    }

    public void setmTitle(String mTitle) {
        this.mTitle = mTitle;
    }

    public String getmPopularity() {
        return mPopularity;
    }

    public void setmPopularity(String mPopularity) {
        this.mPopularity = mPopularity;
    }


    public String getmOverView() {
        return mOverView;
    }

    public void setmOverView(String mOverView) {
        this.mOverView = mOverView;
    }


    public MoviesEntity(){

    }


    public MoviesEntity (int id,String title,String popularity,String overview) {
        mId=id;
        mTitle=title;
        mPopularity=popularity;
        mOverView=overview;
    }

    }



