package com.android.example.github.view.UI;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.example.github.R;
import com.android.example.github.Utils.AutoClearedValue;
import com.android.example.github.Utils.Resource;
import com.android.example.github.binding.FragmentDataBindingComponent;
import com.android.example.github.di.Injectable;
import com.android.example.github.service.model.Movies;
import com.android.example.github.databinding.FragmentMoviesDetailsBinding;
//import com.android.example.github.view.adapter.MovieDetailsAdapter;
import com.android.example.github.view.adapter.MovieDetailsAdapter;
import com.android.example.github.view.adapter.MoviesAdapter;
import com.android.example.github.view.callback.MoviesClickCallback;
import com.android.example.github.viewmodel.SharedViewModel;

import java.util.Collections;

import javax.inject.Inject;

public class MoviesFragment extends Fragment implements Injectable {

    private static final String KEY_Movies_ID = "Movies_id";
    private FragmentMoviesDetailsBinding binding;
    private SharedViewModel viewModel;
    private MoviesClickCallback moviesClickCallback;
    private DataBindingComponent dataBindingComponent = new FragmentDataBindingComponent(this);
    @Inject
    ViewModelProvider.Factory viewModelFactory;
    AutoClearedValue<MovieDetailsAdapter> adapter;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movies_details, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = ViewModelProviders.of(this, viewModelFactory).get(SharedViewModel.class);

        initRecyclerView();
        MovieDetailsAdapter rvAdapter = new MovieDetailsAdapter(dataBindingComponent);
        // binding.moviesDetail.setAdapter(rvAdapter);
        adapter = new AutoClearedValue<>(this, rvAdapter);
        viewModel.setId("b8b9e99d11eb150d1abc559f9056a344", "en-US", 1);
    }


    private void initRecyclerView() {
        Log.d("initRecyclerview", "activity");
        binding.moviesDetail.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager = (LinearLayoutManager)
                        recyclerView.getLayoutManager();
                int lastPosition = layoutManager
                        .findLastVisibleItemPosition();
                if (lastPosition == adapter.get().getItemCount() - 1) {
                    // viewModel.loadNextPage();
                }
            }
        });


        viewModel.getResults().observe(this, result -> {
            Log.d("getResults", "activity");
            if (result != null && result.data != null) {
                adapter.get().replace(result.data);
            } else {
                adapter.get().replace(Collections.emptyList());
            }
        });
    }

    public static MoviesFragment forMovies(String MoviesID) {
        MoviesFragment fragment = new MoviesFragment();
        Bundle args = new Bundle();
        args.putString(KEY_Movies_ID, MoviesID);
        fragment.setArguments(args);
        return fragment;
    }
}



