package com.android.example.github.view.UI;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import com.android.example.github.R;
import com.android.example.github.service.model.Movies;
import com.android.example.github.ui.Common.NavigationController;
import javax.inject.Inject;

import dagger.android.DispatchingAndroidInjector;
import dagger.android.support.HasSupportFragmentInjector;


public class MainActivity extends AppCompatActivity  implements HasSupportFragmentInjector{


    @Inject
    DispatchingAndroidInjector<Fragment> dispatchingAndroidInjector;

    @Inject
    NavigationController navigationController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.d("activityes","activity");
        if (savedInstanceState == null) {
            MoviesListFragment moviesListFragment = new MoviesListFragment();

            FragmentManager fragmentManager = getSupportFragmentManager();
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragment_container, new                                       MoviesListFragment());
            fragmentTransaction.commit();

        }
    }

    @Override
    public DispatchingAndroidInjector<Fragment> supportFragmentInjector() {
        Log.d("dispatchandroidinjector","activity");
        return dispatchingAndroidInjector;
    }

    public void show(Movies.Results movies) {
        Log.d("show","activity");
     MoviesFragment moviesFragment = MoviesFragment.forMovies(movies.getId().toString());
           getSupportFragmentManager()
                .beginTransaction()
                .addToBackStack("Movies")
                  .replace(R.id.fragment_container, moviesFragment, null).commit();
    }
}
