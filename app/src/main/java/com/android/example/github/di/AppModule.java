/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.example.github.di;

import android.app.Application;
import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.migration.Migration;
import android.util.Log;

//import com.android.example.github.api.GithubService;
//import com.android.example.github.db.GithubDb;
//import com.android.example.github.db.RepoDao;
//import com.android.example.github.db.UserDao;
import com.android.example.github.service.Persistence.MoviesDao;
import com.android.example.github.service.Persistence.MoviesDatabase;
import com.android.example.github.service.repository.ApiInterface;
import com.android.example.github.Utils.LiveDataCallAdapterFactory;
//import com.android.example.github.service.Persistence.MoviesDatabase;
//import com.android.example.github.Utils.util.LiveDataCallAdapterFactory;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

@Module(includes = ViewModelModule.class)
class AppModule {
    @Singleton @Provides
    ApiInterface provideGithubService() {
        Log.d("providegithubservice","search");
        return new Retrofit.Builder()
                .baseUrl(ApiInterface.USGS_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(new LiveDataCallAdapterFactory())
                .build()
                .create(ApiInterface.class);
    }

   @Singleton @Provides
    MoviesDatabase provideDb(Application app) {
       Log.d("providedb","search");

       final Migration MIGRATION_1_2 = new Migration(1, 2) {
           @Override
           public void migrate(SupportSQLiteDatabase database) {
               database.execSQL("ALTER TABLE movieResults "
                       + " ADD COLUMN overview TEXT");
           }
       };

       final Migration MIGRATION_2_3 = new Migration(3,4) {
           @Override
           public void migrate(SupportSQLiteDatabase database) {
               database.execSQL("ALTER TABLE movieResults "
                       + " ADD COLUMN overview TEXT");

           }
       };
       return Room.databaseBuilder(app, MoviesDatabase.class,"movies.db")    .addMigrations(MIGRATION_1_2,MIGRATION_2_3).build();

       }

    @Singleton @Provides
    MoviesDao provideRepoDao(MoviesDatabase db) {
        Log.d("providerepodao","search");
        return db.moviesDao();
    }


}
