package com.android.example.github.di;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;

//import com.android.example.github.ui.repo.RepoViewModel;
//import com.android.example.github.ui.search.SearchViewModel;
//import com.android.example.github.ui.user.UserViewModel;
import com.android.example.github.viewmodel.MoviesViewModelFactory;
import com.android.example.github.viewmodel.SharedViewModel;


import dagger.Binds;
import dagger.Module;
import dagger.multibindings.IntoMap;

@Module

abstract class ViewModelModule {
   /* @Binds
    @IntoMap
    @ViewModelKey(MoviesViewModel.class)
    abstract ViewModel bindMovieViewModel(MoviesViewModel searchViewModel);*/

    @Binds
    @IntoMap
    @ViewModelKey(SharedViewModel.class)
    abstract ViewModel bindMoviesListViewModel(SharedViewModel sharedViewModel);

    @Binds
    abstract ViewModelProvider.Factory bindViewModelFactory(MoviesViewModelFactory factory);
}
