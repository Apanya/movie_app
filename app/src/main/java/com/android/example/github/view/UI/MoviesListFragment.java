package com.android.example.github.view.UI;

import android.arch.lifecycle.Lifecycle;
import android.arch.lifecycle.ViewModelProvider;
import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingComponent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.example.github.R;
import com.android.example.github.binding.FragmentDataBindingComponent;
import com.android.example.github.di.Injectable;
import com.android.example.github.service.model.Movies;
import com.android.example.github.ui.Common.NavigationController;
import com.android.example.github.Utils.AutoClearedValue;
import com.android.example.github.view.adapter.MoviesAdapter;
import com.android.example.github.view.callback.MoviesClickCallback;
import com.android.example.github.viewmodel.SharedViewModel;
import com.android.example.github.databinding.FragmentMoviesListBinding;

import java.util.Collections;

import javax.inject.Inject;

public class MoviesListFragment extends Fragment implements Injectable {
    public static final String TAG = "MoviesListFragment";
    private FragmentMoviesListBinding binding;
    private SharedViewModel viewModel;
    private DataBindingComponent dataBindingComponent=new FragmentDataBindingComponent(this);

    @Inject
    NavigationController navigationController;

    AutoClearedValue<MoviesAdapter> adapter;

    @Inject
    ViewModelProvider.Factory viewModelFactory;

    @Override
    public void onActivityCreated( Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Log.d("movieslistfragment","activity");
        viewModel = ViewModelProviders.of(this,viewModelFactory).get (SharedViewModel.class);

        initRecyclerView();
        MoviesAdapter rvAdapter = new MoviesAdapter(dataBindingComponent,
               moviesClickCallback);
        binding.moviesList.setAdapter(rvAdapter);
        adapter = new AutoClearedValue<>(this, rvAdapter);
        viewModel.setId("b8b9e99d11eb150d1abc559f9056a344", "en-US",1);

        }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_movies_list,                  container,false,dataBindingComponent);
        binding.setRetryCallback(()->viewModel.retry());
       // binding = new AutoClearedValue<>(this, binding);
        binding.setIsLoading(true);
        return binding.getRoot();
        }

    private void initRecyclerView() {
        Log.d("initRecyclerview","activity");
        binding.moviesList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                LinearLayoutManager layoutManager = (LinearLayoutManager)
                        recyclerView.getLayoutManager();
                int lastPosition = layoutManager
                        .findLastVisibleItemPosition();
                if (lastPosition == adapter.get().getItemCount() - 1) {
                   // viewModel.loadNextPage();
                }
            }
        });


        viewModel.getResults().observe(this, result -> {
            Log.d("getResults","activity");
            if (result != null && result.data != null) {
                binding.setListResource(result);
                adapter.get().replace(result.data);
            } else {

                //noinspection ConstantConditions
                adapter.get().replace(Collections.emptyList());
            }
        });
    }

    private final MoviesClickCallback moviesClickCallback = new MoviesClickCallback() {
        @Override
        public void onClick(Movies.Results movies) {

            Log.d("onclick","activity");
            if (getLifecycle().getCurrentState().isAtLeast(Lifecycle.State.STARTED)) {
                ((MainActivity) getActivity()).show(movies);
            }
        }
    };


    }

